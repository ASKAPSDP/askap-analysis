set ( CASACORE_ROOT_DIR     $ENV{CASACORE_ROOT_DIR}                     CACHE  PATH     "Path to the CASACORE root"         FORCE )
set ( COMPONENTS_ROOT_DIR   $ENV{COMPONENTS_ROOT_DIR}                   CACHE  PATH     "Path to the CASA components root"  FORCE )
set ( log4cxx_ROOT_DIR      $ENV{log4cxx_ROOT_DIR}                      CACHE  PATH     "Path to the log4cxx root"          FORCE )
set ( CPPUNIT_ROOT_DIR      $ENV{CPPUNIT_ROOT_DIR}                      CACHE  PATH     "Path to the CPPUNIT root"          FORCE )
set ( XercesC_ROOT_DIR      $ENV{XercesC_ROOT_DIR}                      CACHE  PATH     "Path to the XercesC includes"      FORCE )
set ( XercesC_INCLUDE_DIR   ${XercesC_ROOT_DIR}/include                 CACHE  PATH     "Path to the XercesC includes"      FORCE )

set ( CMAKE_CXX_COMPILER    "/usr/local/opt/llvm/bin/clang++"             CACHE  STRING   "Specify the C++ compiler"            FORCE )
set ( CMAKE_C_COMPILER      "/usr/local/opt/llvm/bin/clang"               CACHE  STRING   "Specify the C compiler"              FORCE )

# A list of specific version for the dependencies of this build.
#
# The contents here will aid in 'pinning' the contents of the build. Changing this file constitutes changing the build
# and this means that the integration version should be promoted. The version numbers (tags) specified here will be
# injected into the CMakeLists.txt file in the External_project_add definitions and will, therefore, control the
# versions of various sub libs built and integrated. They need not be official versions and can be arbitrary branches or
# tags such as dev feature branches etc.
#
# You can affect the build content by manipulating the "tag/branch" column. Should be something that git expects for that 
# repo.

#     Identifier                   tag/branch       cache  type      description                        force it
set ( ASKAP_CMAKE_TAG              develop       CACHE  STRING    "askap.cmake tools"                FORCE )
set ( ASKAP_DEV_TAG                develop       CACHE  STRING    "askap dev tools"                  FORCE )
set ( LOFAR_COMMON_TAG             develop       CACHE  STRING    "lofar-common version"             FORCE )
set ( LOFAR_BLOB_TAG               develop       CACHE  STRING    "lofar-blob version"               FORCE )
set ( BASE_ASKAP_TAG               develop       CACHE  STRING    "base-askap version"               FORCE )
set ( BASE_LOGFILTERS_TAG          develop       CACHE  STRING    "base-logfilters version"          FORCE )
set ( BASE_IMAGEMATH_TAG           develop       CACHE  STRING    "base-imagemath version"           FORCE )
set ( BASE_ASKAPPARRALLEL_TAG      develop       CACHE  STRING    "base-askapparrallel version"      FORCE )
set ( BASE_SCIMATH_TAG             develop       CACHE  STRING    "base-scimath version"             FORCE )
set ( BASE_ACCESSORS_TAG           develop       CACHE  STRING    "base-accessors version"           FORCE )
set ( BASE_COMPONENTS_TAG          develop       CACHE  STRING    "base-components version"          FORCE )
 
# TOS related repos are not versioned yet, so pinned this build with commit hash values.
set ( PYTHON_ASKAP_TAG             3c4871a07d7cdf8a71871074cd90e8b3bf8d16de CACHE  STRING    "python-askap (tos) version"       FORCE )
set ( PYTHON_ICEUTILS_TAG          8e34898ef30d530648b02cf81ca8e6e3c9d0781e CACHE  STRING    "python-iceutils (tos) version"    FORCE )
set ( PYTHON_LOGHANDLERS_TAG       e3b682ef6a79ab14943789ad5ef6e5a46c4d7bb7 CACHE  STRING    "python-loghandlers (tos) version" FORCE )
set ( PYTHON_PARSET_TAG            e6bfeb00e5b3a9e5a8aae20db48a1e6470aa1c35 CACHE  STRING    "python-parset (tos) version"      FORCE )
