cmake_minimum_required (VERSION 3.12.0)

include_guard()

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(VERSION "1.15.0")
project(askap-analysis VERSION ${VERSION} LANGUAGES C CXX)

find_package(Git REQUIRED)

include(CheckCXXCompilerFlag)
include (GNUInstallDirs)
# Load the version IDs of all the dependency sub repos
include(dependencies.cmake)

message (STATUS "Forming build label")
execute_process(
   COMMAND git describe --tags --always --dirty
   OUTPUT_VARIABLE BUILD_STR
   OUTPUT_STRIP_TRAILING_WHITESPACE
)
message(STATUS "Build label is ${BUILD_STR}")

message (STATUS "DEV_OVERRIDES = $ENV{DEV_OVERRIDES}")
if(DEFINED ENV{DEV_OVERRIDES})
  message (STATUS "cmake config overrides file specified, $ENV{DEV_OVERRIDES}")
  include($ENV{DEV_OVERRIDES} OPTIONAL)
else()
  message (STATUS "No cmake config overrides file specified, using defaults")
endif()

message(STATUS "CMAKE_INSTALL_PREFIX is ${CMAKE_INSTALL_PREFIX}")

# Get a remote git repo url, either already set from config, or from current git dir.
message (STATUS "Determining remote git url prefix ...")
if (NOT ASKAPSDP_GIT_URL)
  message (STATUS "Remote git url prefix not specified with ASKAPSDP_GIT_URL value, using current git dir ...")
  execute_process(
     COMMAND git config --get remote.origin.url
     COMMAND xargs dirname
     OUTPUT_VARIABLE ASKAPSDP_GIT_URL
     OUTPUT_STRIP_TRAILING_WHITESPACE
  )
  if (NOT ASKAPSDP_GIT_URL)
     message(FATAL_ERROR
     "Could not determine a remote git url to retrieve dependencies. This can be because we are not in a git dir or you need to set ASKAPSDP_GIT_URL var in your dev-overrides or from the cmake command line with '-DASKAPSDP_GIT_URL=<repo-prefix>'.")
  endif()
endif()
message(STATUS "Remote git url prefix is ${ASKAPSDP_GIT_URL}")

# Declare all of sub repos and tools
include(FetchContent)

# Put our askap cmake modules in the cmake path
# We need this before any other Fetch calls so we have the find_package routines available
# -------- askap cmake modules --------
FetchContent_Declare(askap-cmake
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/askap-cmake.git
  GIT_TAG           ${ASKAP_CMAKE_TAG}
  SOURCE_DIR        askap-cmake
)

message (STATUS "Fetching askap-cmake files")
FetchContent_GetProperties(askap-cmake)
if(NOT askap-cmake_POPULATED)
  FetchContent_Populate(askap-cmake)

endif()
list(APPEND CMAKE_MODULE_PATH "${askap-cmake_SOURCE_DIR}")
#message(STATUS "askap-cmake path is ${askap-cmake_SOURCE_DIR}")
message(STATUS "askap-cmake path is ${CMAKE_MODULE_PATH}")

include(version_utils)

# -------- lofar common --------
FetchContent_Declare(lofar-common
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/lofar-common.git
  GIT_TAG           ${LOFAR_COMMON_TAG}
  SOURCE_DIR        lofar-common
)

# -------- lofar blob --------
FetchContent_Declare(lofar-blob
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/lofar-blob.git
  GIT_TAG           ${LOFAR_BLOB_TAG}
  SOURCE_DIR        lofar-blob
)

# -------- base-askap --------
FetchContent_Declare(base-askap
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/base-askap.git
  GIT_TAG           ${BASE_ASKAP_TAG}
  SOURCE_DIR        base-askap
)

# -------- base-askapparallel --------
FetchContent_Declare(base-askapparallel
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/base-askapparallel.git
  GIT_TAG           ${BASE_ASKAPPARALLEL_TAG}
  SOURCE_DIR        base-askapparallel
)

# -------- base-imagemath --------
FetchContent_Declare(base-imagemath
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/base-imagemath.git
  GIT_TAG           ${BASE_IMAGEMATH_TAG}
  SOURCE_DIR        base-imagemath
)

# -------- base-scimath --------
FetchContent_Declare(base-scimath
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/base-scimath.git
  GIT_TAG           ${BASE_SCIMATH_TAG}
  SOURCE_DIR        base-scimath
)

# -------- base-accessors --------
FetchContent_Declare(base-accessors
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/base-accessors.git
  GIT_TAG           ${BASE_ACCESSORS_TAG}
  SOURCE_DIR        base-accessors
)

# -------- base-components --------
FetchContent_Declare(base-components
  GIT_REPOSITORY    ${ASKAPSDP_GIT_URL}/base-components.git
  GIT_TAG           ${BASE_COMPONENTS_TAG}
  SOURCE_DIR        base-components
)

message (STATUS "Fetching sub repos")
FetchContent_MakeAvailable( lofar-common lofar-blob
                           base-askap base-imagemath base-askapparallel base-scimath
                           base-accessors base-components
)
message (STATUS "Done - Fetching sub repos")

# configure a header file to pass some of the CMake settings
# to the source code
configure_file (
	askap_analysis.cc.in ${CMAKE_CURRENT_BINARY_DIR}/askap/analysis/askap_analysis.cc)

configure_file (
	askap_analysisutilities.cc.in ${CMAKE_CURRENT_BINARY_DIR}/askap/analysisutilities/askap_analysisutilities.cc )


include_directories("${PROJECT_SOURCE_DIR}/askap/analysisutilities")
include_directories("${PROJECT_SOURCE_DIR}/askap/analysis")

option (CXX11 "Compile as C++11 if possible" NO)
option (ENABLE_SHARED "Build shared libraries" YES)
option (ENABLE_RPATH "Include rpath in executables and shared libraries" YES)

include(CTest)

include(ExternalProject)
set(EXTERNAL_INSTALL_LOCATION ${CMAKE_CURRENT_BINARY_DIR}/external)

include_directories(${EXTERNAL_INSTALL_LOCATION}/include)
link_directories(${EXTERNAL_INSTALL_LOCATION}/lib)

# uninstall target
if(NOT TARGET uninstall)
    configure_file(
        "${CMAKE_CURRENT_SOURCE_DIR}/cmake_uninstall.cmake.in"
        "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake"
        IMMEDIATE @ONLY)

    add_custom_target(uninstall
        COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake)
endif()

if (ENABLE_SHARED)
option (BUILD_SHARED_LIBS "" YES)
    if (ENABLE_RPATH)
        # Set RPATH to use for installed targets; append linker search path
        set(CMAKE_INSTALL_NAME_DIR "${CMAKE_INSTALL_PREFIX}/lib" )
        set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
        set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
    endif (ENABLE_RPATH)
endif(ENABLE_SHARED)

# Yanda depends

# Yanda Packages
# YandaSoft dependencies .. in order
#find_package(lofar-common REQUIRED)
#find_package(lofar-blob REQUIRED)
#find_package(askap-askap REQUIRED)
#find_package(askap-scimath REQUIRED)
#find_package(askap-parallel REQUIRED)
#find_package(askap-accessors REQUIRED)
#find_package(askap-components REQUIRED)

# find packages
find_package(Boost REQUIRED COMPONENTS regex filesystem)
find_package(log4cxx REQUIRED)
find_package(Casacore REQUIRED COMPONENTS  ms images mirlib coordinates fits lattices measures scimath scimath_f tables casa)
find_package(Components)
find_package(MPI REQUIRED)
find_package(CFITSIO REQUIRED)
find_package(Duchamp) 

find_package(GSL REQUIRED)
find_package(CPPUnit REQUIRED)

if (CASACORE3)
	add_definitions(-DHAVE_CASACORE3)
endif (CASACORE3)

if (NOT GSL_VERSION VERSION_LESS 2.0)
	add_definitions(-DHAVE_GSL2)
endif()

include_directories(${MPI_INCLUDE_PATH})

if (CXX11)
	set(CMAKE_CXX_STANDARD 11)
	set(CMAKE_CXX_STANDARD_REQUIRED ON)
endif()


if (CPPUNIT_FOUND)
	include_directories(${CPPUNIT_INCLUDE_DIRS})
endif (CPPUNIT_FOUND)


ExternalProject_Add(busyfit
    GIT_REPOSITORY https://bitbucket.csiro.au/scm/askapsdp/busyfit.git
    SOURCE_DIR ${EXTERNAL_INSTALL_LOCATION}/busyfit
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${EXTERNAL_INSTALL_LOCATION}
)

if (NOT DUCHAMP_FOUND)
ExternalProject_Add(duchamp
    SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/duchamp/Duchamp-1.6.3
    CONFIGURE_COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/duchamp/Duchamp-1.6.3/configure --with-cfitsio=${CFITSIO_INCLUDE_DIR}/.. --with-wcslib=${WCSLIB_INCLUDE_DIR}/.. --without-pgplot --prefix=${EXTERNAL_INSTALL_LOCATION}
    BUILD_COMMAND  /usr/bin/make duchamp /usr/bin/make lib
    BUILD_IN_SOURCE 1
    INSTALL_COMMAND /usr/bin/make install /usr/bin/make cleanest
)
    set (DUCHAMP_LIBRARY duchamp-1.6.3)
endif ()

if (DUCHAMP_FOUND)
    add_custom_target(duchamp)	
    include_directories(${DUCHAMP_INCLUDE_DIR})
endif(DUCHAMP_FOUND)

add_library(analysis SHARED)

target_include_directories(analysis PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/askap/scimath>
  $<INSTALL_INTERFACE:include>
  $<INSTALL_INTERFACE:include/askap/scimath>
  ${Boost_INCLUDE_DIRS}
  ${log4cxx_INCLUDE_DIRS}
  ${CASACORE_INCLUDE_DIRS}
  ${COMPONENTS_INCLUDE_DIRS}
  ${CFITSIO_INCLUDE_DIRS}
  ${GSL_INCLUDE_DIRS}
)


macro(add_sources_to_analysis)
    foreach(arg IN ITEMS ${ARGN})
        target_sources(analysis PRIVATE ${CMAKE_CURRENT_LIST_DIR}/${arg})
    endforeach()
endmacro()
target_compile_definitions(analysis PUBLIC
        casa=casacore
        HAVE_AIPSPP
        HAVE_BOOST
        HAVE_LOG4CXX
)
# add some more tests and sub-directories

set_target_properties(analysis PROPERTIES
        OUTPUT_NAME askap_analysis
)

add_subdirectory(askap/analysisutilities)
add_subdirectory(askap/analysis) 
add_subdirectory(askap/simulations) 

add_dependencies(analysis
	busyfit
	duchamp
)
target_link_libraries(analysis PUBLIC
		BusyFit-dev
        ${log4cxx_LIBRARY}
		${CASACORE_LIBRARIES}
	 	${FFTW_LIBRARIES}
		${Boost_LIBRARIES}
		${COMPONENTS_LIBRARY}
		${DUCHAMP_LIBRARY}
		${GSL_LIBRARY}
		askap::askap
        askap::components
		askap::scimath
        askap::imagemath
        askap::accessors
        askap::parallel
        lofar::Common
        lofar::Blob
)
if (CPPUNIT_FOUND)
	include(CTest)
    enable_testing()
	add_subdirectory(tests/analysisutilities/analysisparallel)
	add_subdirectory(tests/analysisutilities/coordutils)
	add_subdirectory(tests/analysisutilities/modelcomponents)
	add_subdirectory(tests/analysis/extraction)
	add_subdirectory(tests/analysis/patternmatching)
	add_subdirectory(tests/analysis/polarisation)
	add_subdirectory(tests/analysis/preprocessing)
	add_subdirectory(tests/analysis/sourcefitting)
endif()	
add_library(askap::analysis ALIAS analysis)
install (
	TARGETS analysis 
    EXPORT askap-analysis-targets
	RUNTIME DESTINATION bin
	LIBRARY DESTINATION lib
	ARCHIVE DESTINATION lib
	LIBRARY PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
)



#include(yanda_export)
#yanda_export(askap)
