#
# askap/simulations/simulationutilities
#
add_sources_to_analysis(
FluxGenerator.cc
SimulationUtilities.cc
)

install (FILES
FluxGenerator.h
SimulationUtilities.h
 DESTINATION include/askap/simulations/simulationutilities
)

