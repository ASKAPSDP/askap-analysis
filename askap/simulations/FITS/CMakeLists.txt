#
# askap/simulations/FITS
#
add_sources_to_analysis(
FITSfile.cc
FITSparallel.cc
)

install (FILES
FITSfile.h
FITSparallel.h
 DESTINATION include/askap/simulations/FITS
)

