#
# askap/analysis/patternmatching
#
add_sources_to_analysis(
CatalogueMatcher.cc
Matcher.cc
MatchingUtilities.cc
Point.cc
PointCatalogue.cc
Side.cc
Triangle.cc
tester.cc
	)

install (FILES
CatalogueMatcher.h
Matcher.h
MatchingUtilities.h
Point.h
PointCatalogue.h
Side.h
Triangle.h
	DESTINATION include/askap/analysis/patternmatching
)

