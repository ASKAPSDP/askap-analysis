/// @file
///
/// Class to handle extraction of a summed spectrum corresponding to a source.
///
/// @copyright (c) 2011 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Matthew Whiting <Matthew.Whiting@csiro.au>
///
#ifndef ASKAP_ANALYSIS_EXTRACTION_SPECTRALBOX_H_
#define ASKAP_ANALYSIS_EXTRACTION_SPECTRALBOX_H_
#include <askap_analysis.h>
#include <askap/analysis/extraction/SourceDataExtractor.h>
#include <Common/ParameterSet.h>
#include <askap/imageaccess/FitsAuxImageSpectra.h>
#include <askap/askapparallel/AskapParallel.h>
#include <memory>
#include <map>

namespace askap {

namespace analysis {

/// @brief The default box width for spectral extraction
const int defaultSpectralExtractionBoxWidth = 5;

/// @brief Class to handle the extraction of some sort of spectrum
/// corresponding to a given RadioSource.

/// @details This is a base class that provides the core

/// functionality to extract a spectrum obtained in some way over
/// a box of a given size centred on the prescribed object. This
/// class defines functions that set up the slicer used to extract
/// the data from the input image, and that write out the
/// resulting spectrum to an image on disk. The details of the
/// extraction (what function to use, how the flux is scaled, etc)
/// is left to derived classes.

class SpectralBoxExtractor : public SourceDataExtractor {
    public:
        SpectralBoxExtractor() = default;

        /// @details Initialise the extractor from a LOFAR parset. This
        /// sets the input cube, the box width, the scaling flag, and
        /// the base name for the output spectra files (these will have
        /// _X appended, where X is the ID of the object in question).
        SpectralBoxExtractor(const LOFAR::ParameterSet& parset,askap::askapparallel::AskapParallel* comms = nullptr);
        virtual ~SpectralBoxExtractor() {};

        int boxWidth() {return itsBoxWidth;};
        virtual void setBoxWidth(int w) {itsBoxWidth = w;};

        virtual void extract() = 0;

        /// @details This method does not write the spectra to the fits file if usefitstable parameter
        ///          is true/set. Instead, it uses the scatterSources() method to send the data back
        ///          to the master which does the actual writing.
        void writeImage();

        /// Return a vector list of frequency values that match the spectrum array.
        casa::Array<Float> frequencies();
        /// Return the frequency units from the spectrum array.
        std::string freqUnit();
        
        /// @details This method should be called by the master. It gathers the component id and
        ///          the spectra from the workers and write them to a fits binary table if the 
        ///          usefitstable parameter in the parset is set to true. Also, the worker
        ///          must send component id of length 0 to indicate that it has finished sending
        ///          otherwise the task will hang 
        void gatherSources();

        void setComms(askap::askapparallel::AskapParallel* comms) { itsComms = comms; }

    protected:
        virtual void defineSlicer();
        void initialiseArray();

        int itsBoxWidth;

    private:
        /// @details This method should be called by the workers to send the component id
        ///          and the spectra to the master if the usefitstable parameter is set/true 
        ///          in the parset
        void scatterSources();

        //std::shared_ptr<askap::accessors::FitsAuxImageSpectra> itsFitsSpectraWriter;
        std::map<std::string,std::shared_ptr<askap::accessors::FitsAuxImageSpectra>> itsWriterMap;
        // dont delete it 
        askap::askapparallel::AskapParallel* itsComms;

};

}
}

#endif
