#
# askap/analysis/extraction
#
add_sources_to_analysis(
CubeletExtractor.cc
ExtractionFactory.cc
HIdata.cc
IslandData.cc
MomentMapExtractor.cc
NoiseSpectrumExtractor.cc
SourceDataExtractor.cc
SourceSpectrumExtractor.cc
SpectralBoxExtractor.cc
)


install (FILES
CubeletExtractor.h
ExtractionFactory.h
HIdata.h
IslandData.h
MomentMapExtractor.h
NoiseSpectrumExtractor.h
SourceDataExtractor.h
SourceSpectrumExtractor.h
SpectralBoxExtractor.h
DESTINATION include/askap/analysis/extraction
)

