/// @file
///
/// Class to handle extraction of a summed spectrum corresponding to a source.
///
/// @copyright (c) 2011 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Matthew Whiting <Matthew.Whiting@csiro.au>
///
#include <askap/analysis/extraction/SpectralBoxExtractor.h>
#include <askap_analysis.h>
#include <askap/analysis/extraction/SourceDataExtractor.h>

#include <askap/askap/AskapLogging.h>
#include <askap/askap/AskapError.h>

#include <askap/analysis/sourcefitting/RadioSource.h>

#include <askap/imageaccess/ImageAccessFactory.h>

#include <casacore/casa/Arrays/IPosition.h>
#include <casacore/casa/Arrays/Array.h>
#include <casacore/casa/Arrays/Slicer.h>
#include <casacore/casa/Arrays/ArrayLogical.h>
#include <casacore/coordinates/Coordinates/CoordinateUtil.h>
#include <casacore/coordinates/Coordinates/CoordinateSystem.h>
#include <casacore/coordinates/Coordinates/DirectionCoordinate.h>
#include <casacore/coordinates/Coordinates/SpectralCoordinate.h>
#include <casacore/coordinates/Coordinates/StokesCoordinate.h>
#include <casacore/measures/Measures/Stokes.h>

#include <Common/ParameterSet.h>
#include <vector>

using namespace askap::analysis::sourcefitting;

ASKAP_LOGGER(logger, ".spectralboxextractor");

namespace askap {

namespace analysis {

SpectralBoxExtractor::SpectralBoxExtractor(const LOFAR::ParameterSet& parset,askap::askapparallel::AskapParallel* comms):
    SourceDataExtractor(parset),itsComms(comms)
{

    itsBoxWidth = parset.getInt16("spectralBoxWidth", defaultSpectralExtractionBoxWidth);

    itsOutputFilenameBase = parset.getString("spectralOutputBase", "");
    ASKAPCHECK(itsOutputFilenameBase != "", "Extraction: " <<
               "No output base name has been provided for the spectral output. " <<
               "Use spectralOutputBase.");

}

void SpectralBoxExtractor::initialiseArray()
{
    // Form itsArray and initialise to zero
    if (this->openInput()) {
        int specsize = itsInputShape(itsSpcAxis);
        casa::IPosition shape(itsInputShape.size(), 1);
        shape(itsSpcAxis) = specsize;
        if (itsStkAxis > -1) {
            shape(itsStkAxis) = itsStokesList.size();
        }
        itsArray = casa::Array<Float>(shape, 0.0f);
        itsMask = casa::LogicalArray(shape, true);
    }
    else {
        ASKAPLOG_ERROR_STR(logger, "Could not open image");
    }
}

void SpectralBoxExtractor::defineSlicer()
{

    if (this->openInput() && itsSourceInBounds) {
        ASKAPLOG_DEBUG_STR(logger, "Shape from input cube = " << itsInputShape);
        ASKAPCHECK(itsInputCoords.hasSpectralAxis(),
                   "Input cube \"" << itsInputCube << "\" has no spectral axis");
        ASKAPCHECK(itsInputCoords.hasDirectionCoordinate(),
                   "Input cube \"" << itsInputCube << "\" has no spatial axes");

        // define the slicer based on the source's peak pixel location and the box width.
        // Make sure we don't go over the edges of the image.
        int xmin, ymin, xmax, ymax;
        if (itsBoxWidth > 0) {
            int hw = (itsBoxWidth - 1) / 2;
            int xloc = int(itsXloc);
            int yloc = int(itsYloc);
            int zero = 0;
            ASKAPLOG_DEBUG_STR(logger, "Problematic bit: xloc=" << xloc << " yloc=" << yloc << " hw=" << hw << " shape(itsLngAxis)=" << itsInputShape(itsLngAxis) << " shape(itsLatAxis)=" << itsInputShape(itsLatAxis));
            xmin = std::max(zero, xloc - hw);
            xmax = std::min(int(itsInputShape(itsLngAxis) - 1), xloc + hw);
            ymin = std::max(zero, yloc - hw);
            ymax = std::min(int(itsInputShape(itsLatAxis) - 1), yloc + hw);
            ASKAPLOG_DEBUG_STR(logger, "Problematic bit 2: xmin=" << xmin << " xmax=" << xmax << " ymin=" << ymin << " ymax=" << ymax);
        }
        else {
            ASKAPASSERT(itsSource);
            // use the detected pixels of the source for the spectral
            // extraction, and the x/y ranges for slicer
            xmin = itsSource->getXmin() + itsSource->getXOffset();
            xmax = itsSource->getXmax() + itsSource->getXOffset();
            ymin = itsSource->getYmin() + itsSource->getYOffset();
            ymax = itsSource->getYmax() + itsSource->getYOffset();
        }

        // Only set the Slicer if we lie within the bounds of the image
        if ((xmin < int(itsInputShape(itsLngAxis))) &&
                (xmax >= 0) &&
                (ymin < int(itsInputShape(itsLatAxis))) &&
                (ymax >= 0)) {

            itsReadyToExtract = true;

            casa::IPosition blc(itsInputShape.size(), 0), trc(itsInputShape.size(), 0);
            blc(itsLngAxis) = xmin;
            blc(itsLatAxis) = ymin;
            blc(itsSpcAxis) = 0;
            trc(itsLngAxis) = xmax;
            trc(itsLatAxis) = ymax;
            trc(itsSpcAxis) = itsInputShape(itsSpcAxis) - 1;
            if (itsStkAxis > -1) {
                casa::Stokes stk;
                blc(itsStkAxis) = trc(itsStkAxis) =
                                      itsInputCoords.stokesPixelNumber(stk.name(itsCurrentStokes));
            }
            ASKAPLOG_DEBUG_STR(logger, "Defining slicer for " << itsInputCube <<
                               " based on blc=" << blc << ", trc=" << trc);
            itsSlicer = casa::Slicer(blc, trc, casa::Slicer::endIsLast);
        }
        else {
            ASKAPLOG_DEBUG_STR(logger, "Extraction box is outside the image boundaries: xmin=" << xmin << " xmax=" << xmax << " ymin=" << ymin << " ymax=" << ymax);
            ASKAPLOG_DEBUG_STR(logger, "No extraction done for this source");
        }

    }
    else {
        ASKAPLOG_ERROR_STR(logger, "Could not open image");
    }
}


void SpectralBoxExtractor::writeImage()
{
    if (openInput() && itsSourceInBounds && itsReadyToExtract) {

        bool useSpectraTable = itsParset.getBool("usefitstable",false);
        if ( !useSpectraTable ) {
            ASKAPLOG_INFO_STR(logger, "Writing spectrum to " << itsOutputFilename);
            const std::string usefitstableParam = (useSpectraTable == true ? "true" : "false");
            ASKAPLOG_DEBUG_STR(logger, "usefitstable: " << usefitstableParam);

            casa::CoordinateSystem newcoo = casa::CoordinateUtil::defaultCoords4D();

            int dirCoNum = itsInputCoords.findCoordinate(casa::Coordinate::DIRECTION);
            int spcCoNum = itsInputCoords.findCoordinate(casa::Coordinate::SPECTRAL);
            int stkCoNum = itsInputCoords.findCoordinate(casa::Coordinate::STOKES);

            casa::DirectionCoordinate dircoo(itsInputCoords.directionCoordinate(dirCoNum));
            casa::SpectralCoordinate spcoo(itsInputCoords.spectralCoordinate(spcCoNum));
            casa::Vector<Int> stkvec(itsStokesList.size());
            for (size_t i = 0; i < stkvec.size(); i++) {
                stkvec[i] = itsStokesList[i];
            }
            casa::StokesCoordinate stkcoo(stkvec);

            newcoo.replaceCoordinate(dircoo, newcoo.findCoordinate(casa::Coordinate::DIRECTION));
            newcoo.replaceCoordinate(spcoo, newcoo.findCoordinate(casa::Coordinate::SPECTRAL));
            if (stkCoNum >= 0) {
                newcoo.replaceCoordinate(stkcoo, newcoo.findCoordinate(casa::Coordinate::STOKES));
            }

            // shift the reference pixel for the spatial coords, so that
            // the RA/DEC (or whatever) are correct. Leave the
            // spectral/stokes axes untouched.
            int lngAxis = newcoo.directionAxesNumbers()[0];
            int latAxis = newcoo.directionAxesNumbers()[1];
            int spcAxis = newcoo.spectralAxisNumber();
            int stkAxis = newcoo.polarizationAxisNumber();
            casa::IPosition outshape(4, 1);
            outshape(spcAxis) = itsSlicer.length()(itsSpcAxis);
            outshape(stkAxis) = stkvec.size();
            casa::Vector<Float> shift(outshape.size(), 0);
            casa::Vector<Float> incrFrac(outshape.size(), 1);
            shift(lngAxis) = itsXloc;
            shift(latAxis) = itsYloc;
            casa::Vector<Int> newshape = outshape.asVector();
            newcoo.subImageInSitu(shift, incrFrac, newshape);

            Array<Float> newarray(itsArray.reform(outshape));
            LogicalArray newmask(itsMask.reform(outshape));

            boost::shared_ptr<accessors::IImageAccess<casacore::Float> > ia = accessors::imageAccessFactory(itsParset);
            ia->create(itsOutputFilename, newarray.shape(), newcoo);

            /// @todo save the new units - if units were per beam, remove this factor

            // write the array
            ia->write(itsOutputFilename, newarray);
            ia->setUnits(itsOutputFilename, itsOutputUnits.getName());

            // write the mask
            ia->writeMask(itsOutputFilename, newmask);

            // update the metadata
            updateHeaders(itsOutputFilename);

            ASKAPLOG_DEBUG_STR(logger, "Writing of " << itsOutputFilename << " is complete");
        } else {
            // usefitstable flag is set. In this case, we want the workers to send the component id
            // and the spectra back to the master and let the master does the writing the spectra
            // to the binary table 
            scatterSources();
        }

    }
}


casa::Array<Float> SpectralBoxExtractor::frequencies()
{
    casa::Vector<Float> freqs;
    if (this->openInput() && itsSpcAxis) {
        freqs = casa::Vector<Float>(itsInputShape(itsSpcAxis), 0.f);
        int spcCoNum = itsInputCoords.findCoordinate(casa::Coordinate::SPECTRAL);
        casa::SpectralCoordinate spcoo(itsInputCoords.spectralCoordinate(spcCoNum));
        for (unsigned int i = 0; i < itsInputShape(itsSpcAxis); i++) {
            Double pix = i;
            Double freq;
            ASKAPCHECK(spcoo.toWorld(freq, pix),
                       "WCS conversion failed in calculating frequencies");
            freqs[i] = freq;
        }
    }
    else ASKAPLOG_ERROR_STR(logger, "Could not open image");

    return freqs;
}

std::string SpectralBoxExtractor::freqUnit()
{
    std::string unit;
    if (this->openInput() && itsSpcAxis) {
        int spcCoNum = itsInputCoords.findCoordinate(casa::Coordinate::SPECTRAL);
        casa::SpectralCoordinate spcoo(itsInputCoords.spectralCoordinate(spcCoNum));
        casa::Vector<casa::String> units = spcoo.worldAxisUnits();
        if (units.size() > 1) {
            ASKAPLOG_WARN_STR(logger, "Multiple units in spectral axis: " << units);
        }
        unit = units[0];
    }
    return unit;
}

void SpectralBoxExtractor::scatterSources()
{
    if ( itsComms != nullptr && itsComms->isWorker() && itsComms->isParallel() ) {
        const int master = 0;
        // send the length of the component id
        //std::size_t len = this->sourceID().size();
        std::size_t len = this->component()->componentID().size();
        itsComms->send(&len,sizeof(std::size_t),master);
        // send the content of the component id
        //const char* data = this->sourceID().data();
        const char* data = this->component()->componentID().data();
        ASKAPLOG_INFO_STR(logger,"source id: " <<  this->component()->componentID().data());
        //itsComms->send(data,len,master);
        itsComms->send(this->component()->componentID().data(),len,master);
        // send the size of the array/spectra
        const std::size_t arraySize = this->array().size();
        itsComms->send(&arraySize,sizeof(std::size_t),master);
        // send the array data
        itsComms->send(this->array().data(),arraySize*sizeof(float),master);
    }
}

void SpectralBoxExtractor::gatherSources()
{
    bool useSpectraTable = itsParset.getBool("usefitstable",false);
    if ( useSpectraTable && itsComms != nullptr && itsComms->isMaster() && itsComms->isParallel() ) {
        ASKAPLOG_INFO_STR(logger,"master: gathering sournces from workers and write to fits table: " << itsOutputFilenameBase);
        std::shared_ptr<askap::accessors::FitsAuxImageSpectra> specWriter;
        const int nProcs = itsComms->nProcs();
        for ( int r = 1; r < nProcs; r++) {
            // stay in the while loop until the worker sends the comp id of length 0 to indicate that
            // it finishes sending
            while ( true ) {
                // receive the length of component
                std::size_t len = 0;
                itsComms->receive(&len,sizeof(std::size_t),r);
                ASKAPLOG_INFO_STR(logger,"master: received comp id len: " << len);
                if ( len != 0 ) {
                    // receive the component data
                    std::vector<char> compName(len);
                    char* data = compName.data();
                    itsComms->receive(data,len,r);
                    std::string compId(data,len);
                    ASKAPLOG_INFO_STR(logger,"master: received comp id: " << compId);
                    // receive the spectral/array size
                    std::size_t sz = 0;
                    itsComms->receive(&sz,sizeof(std::size_t),r);
                    // receive the spectra data
                    //std::vector<float> spectra(sz);
                    casacore::Vector<float> spectra(sz);
                    float* spectraData = spectra.data();
                    itsComms->receive(spectraData,sizeof(float)*sz,r);
                    auto iter = itsWriterMap.find(itsOutputFilenameBase);
                    //if ( !itsFitsSpectraWriter && itsComms->isMaster() ) {
                    if ( iter == itsWriterMap.end()) {
                        ASKAPLOG_INFO_STR(logger,"itsOutputFilenameBase: " << itsOutputFilenameBase);
                        specWriter.reset(new askap::accessors::FitsAuxImageSpectra(itsOutputFilenameBase,sz,0,itsInputCoords));
                        itsWriterMap.insert(std::make_pair(itsOutputFilenameBase,specWriter));
                    } else {
                        specWriter = iter->second;
                    }
                    // write to spectra to the fits table
                    specWriter->add(compId,spectra);
                } else {
                    // len = 0 means that a worker stops sending the data to the master. @see scatterSources()
                   break; // out of the while loop
                }
            }
        }
    }
}

}

}
