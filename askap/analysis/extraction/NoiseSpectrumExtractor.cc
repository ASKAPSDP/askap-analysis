/// @file
///
/// Class to handle extraction of a summed spectrum corresponding to a source.
///
/// @copyright (c) 2011 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Matthew Whiting <Matthew.Whiting@csiro.au>
///
#include <askap/analysis/extraction/NoiseSpectrumExtractor.h>
#include <askap_analysis.h>
#include <askap/analysis/extraction/SourceDataExtractor.h>
#include <askap/analysis/extraction/SpectralBoxExtractor.h>

#include <askap/askap/AskapLogging.h>
#include <askap/askap/AskapError.h>

#include <askap/analysis/sourcefitting/RadioSource.h>

#include <askap/imageaccess/ImageAccessFactory.h>

#include <casacore/casa/Arrays/IPosition.h>
#include <casacore/casa/Arrays/Array.h>
#include <casacore/casa/Arrays/Slicer.h>
#include <casacore/casa/Arrays/ArrayPartMath.h>
#include <casacore/coordinates/Coordinates/CoordinateSystem.h>
#include <casacore/coordinates/Coordinates/DirectionCoordinate.h>
#include <casacore/measures/Measures/Stokes.h>

#include <Common/ParameterSet.h>

#include <duchamp/Utils/Statistics.hh>

using namespace askap::analysis::sourcefitting;

ASKAP_LOGGER(logger, ".noiseSpectrumExtractor");

namespace askap {

namespace analysis {

NoiseSpectrumExtractor::NoiseSpectrumExtractor(const LOFAR::ParameterSet& parset,askap::askapparallel::AskapParallel* comms)
    : SpectralBoxExtractor(parset,comms)
{

    itsAreaInBeams = parset.getFloat("noiseArea", 50);
    itsRobustFlag = parset.getBool("robust", true);
    // the following is a fallback
    itsBoxWidth = parset.getInt16("spectralBoxWidth", defaultNoiseExtractionBoxWidth);

    casa::Stokes stk;
    itsCurrentStokes = itsStokesList[0];
    itsInputCube = itsCubeStokesMap[itsCurrentStokes];
    if (itsStokesList.size() > 1) {
        ASKAPLOG_WARN_STR(logger, "Noise Extractor: " <<
                          "Will only use the first provided Stokes parameter: " <<
                          stk.name(itsCurrentStokes));
        itsStokesList = casa::Vector<casa::Stokes::StokesTypes>(1, itsCurrentStokes);
        itsCubeStokesMap.clear();
        itsCubeStokesMap[itsCurrentStokes] = itsInputCube;
    }

    this->initialiseArray();
    this->setBoxWidth();

}

void NoiseSpectrumExtractor::setBoxWidth()
{

    if (this->openInput()) {
        ASKAPLOG_DEBUG_STR(logger, "Beam for input cube = " << itsInputBeam);
        if ((itsInputBeam.size() > 0) && (itsInputBeam[0].getValue() > 0.)) {

            int dirCoNum = itsInputCoords.findCoordinate(casa::Coordinate::DIRECTION);
            casa::DirectionCoordinate dirCoo = itsInputCoords.directionCoordinate(dirCoNum);
            double fwhmMajPix = itsInputBeam[0].getValue(dirCoo.worldAxisUnits()[0]) /
                                fabs(dirCoo.increment()[0]);
            double fwhmMinPix = itsInputBeam[1].getValue(dirCoo.worldAxisUnits()[1]) /
                                fabs(dirCoo.increment()[1]);
            double beamAreaInPix = M_PI * fwhmMajPix * fwhmMinPix;

            itsBoxWidth = int(ceil(sqrt(itsAreaInBeams * beamAreaInPix)));

            ASKAPLOG_INFO_STR(logger, "Noise Extractor: Using box of area " <<
                              itsAreaInBeams << " beams (each of area " <<
                              beamAreaInPix << " pix), or a square of " <<
                              itsBoxWidth << " pix on the side");

        }
        else {

            ASKAPLOG_WARN_STR(logger, "Input image \"" << itsInputCube <<
                              "\" has no beam information. " <<
                              "Using box width value from parset of " <<
                              itsBoxWidth << "pix");
        }

    }
    else ASKAPLOG_ERROR_STR(logger, "Could not open image");
}


void NoiseSpectrumExtractor::extract()
{

    this->defineSlicer();
    if (this->openInput() && itsReadyToExtract) {

        ASKAPLOG_INFO_STR(logger, "Extracting noise spectrum from " << itsInputCube <<
                          " surrounding source ID " << itsSourceID <<
                          " with slicer " << itsSlicer);

        boost::shared_ptr<accessors::IImageAccess<casacore::Float> > ia = accessors::imageAccessFactory(itsParset);
        casa::Array<Float> subarray(ia->read(itsInputCube, itsSlicer.start(), itsSlicer.end()));
        casa::LogicalArray submask(ia->readMask(itsInputCube, itsSlicer.start(), itsSlicer.end()));
        casa::Array<Int> submaskint(submask.shape(), 0);
        casa::LogicalArray::iterator iterM = submask.begin();
        casa::Array<Int>::iterator iterI = submaskint.begin();
        while (iterM != submask.end()) {
            if (*iterM) *iterI = 1;
            iterM++;
            iterI++;
        }

        ASKAPLOG_DEBUG_STR(logger, "subarray.shape = " << subarray.shape());

        casa::IPosition outBLC(itsArray.ndim(), 0);
        casa::IPosition outTRC(itsArray.shape() - 1);

        casa::Array<Float> noisearray;
        if (itsRobustFlag) {
            noisearray = partialMadfms(subarray, IPosition(2, 0, 1)) / Statistics::correctionFactor;
        }
        else {
            noisearray = partialRmss(subarray, IPosition(2, 0, 1));
        }
        itsArray(outBLC, outTRC) = noisearray.reform(itsArray(outBLC, outTRC).shape());

        // sum the mask values over the box and compare to box size
        // if one or more pixel is masked, the normalised sum will end up different (hence masked)
        casa::Array<Int> summaskint = partialSums(submaskint, IPosition(2, 0, 1));
        int boxsize = submaskint.shape()[0] * submaskint.shape()[1];
        casa::Array<Int> reference(summaskint.shape(), boxsize);
        casa::LogicalArray noisemask = (summaskint == reference);
        itsMask(outBLC, outTRC) = noisemask.reform(itsMask(outBLC, outTRC).shape());

    }
    else {
        ASKAPLOG_ERROR_STR(logger, "Could not open image");
    }
}


}

}
