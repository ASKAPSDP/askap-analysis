/// @file
///
/// XXX Notes on program XXX
///
/// @copyright (c) 2011 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Matthew Whiting <Matthew.Whiting@csiro.au>
///
#include <askap/analysis/extraction/CubeletExtractor.h>
#include <askap_analysis.h>
#include <askap/analysis/extraction/SourceDataExtractor.h>

#include <askap/askap/AskapLogging.h>
#include <askap/askap/AskapError.h>

#include <askap/analysis/sourcefitting/RadioSource.h>

#include <askap/imageaccess/ImageAccessFactory.h>

#include <casacore/casa/Arrays/IPosition.h>
#include <casacore/casa/Arrays/Array.h>
#include <casacore/casa/Arrays/Slicer.h>
#include <casacore/coordinates/Coordinates/CoordinateUtil.h>
#include <casacore/coordinates/Coordinates/CoordinateSystem.h>
#include <casacore/coordinates/Coordinates/DirectionCoordinate.h>
#include <casacore/coordinates/Coordinates/SpectralCoordinate.h>
#include <casacore/coordinates/Coordinates/StokesCoordinate.h>
#include <casacore/measures/Measures/Stokes.h>

#include <Common/ParameterSet.h>

using namespace askap::analysis::sourcefitting;

ASKAP_LOGGER(logger, ".cubeletextractor");

namespace askap {

namespace analysis {

CubeletExtractor::CubeletExtractor(const LOFAR::ParameterSet& parset):
    SourceDataExtractor(parset)
{
    std::vector<unsigned int>
    padsizes = parset.getUintVector("padSize", std::vector<unsigned int>(2, 5));

    if (padsizes.size() > 2) {
        ASKAPLOG_WARN_STR(logger,
                          "Only using the first two elements of the padSize vector");
    }

    itsSpatialPad = padsizes[0];
    if (padsizes.size() > 1) {
        itsSpectralPad = padsizes[1];
    }
    else {
        itsSpectralPad = padsizes[0];
    }

    itsOutputFilenameBase = parset.getString("cubeletOutputBase", "");
}

void CubeletExtractor::defineSlicer()
{

    if (this->openInput() && itsSourceInBounds) {
        casa::IPosition blc(itsInputShape.size(), 0);
        casa::IPosition trc = itsInputShape - 1;

        long zero = 0;
        blc(itsLngAxis) = std::max(zero, itsSource->getXmin() - itsSpatialPad + itsSource->getXOffset());
        blc(itsLngAxis) = std::min(blc(itsLngAxis), itsInputShape(itsLngAxis) - 1);
        blc(itsLatAxis) = std::max(zero, itsSource->getYmin() - itsSpatialPad + itsSource->getYOffset());
        blc(itsLatAxis) = std::min(blc(itsLatAxis), itsInputShape(itsLatAxis) - 1);
        if (itsSpcAxis >= 0) {
            blc(itsSpcAxis) = std::max(zero, itsSource->getZmin() - itsSpectralPad + itsSource->getZOffset());
            blc(itsSpcAxis) = std::min(blc(itsSpcAxis), itsInputShape(itsSpcAxis) - 1);
        }

        trc(itsLngAxis) = std::min(itsInputShape(itsLngAxis) - 1,
                                   itsSource->getXmax() + itsSpatialPad + itsSource->getXOffset());
        trc(itsLngAxis) = std::max(zero, trc(itsLngAxis));
        trc(itsLatAxis) = std::min(itsInputShape(itsLatAxis) - 1,
                                   itsSource->getYmax() + itsSpatialPad + itsSource->getYOffset());
        trc(itsLatAxis) = std::max(zero, trc(itsLatAxis));
        if (itsSpcAxis >= 0) {
            trc(itsSpcAxis) = std::min(itsInputShape(itsSpcAxis) - 1,
                                       itsSource->getZmax() + itsSpectralPad + itsSource->getZOffset());
            trc(itsSpcAxis) = std::max(zero, trc(itsSpcAxis));
        }
        /// @todo Not yet dealing with Stokes axis properly.

        bool isGood = (trc(itsLngAxis) >= blc(itsLngAxis));
        isGood = isGood && (trc(itsLatAxis) >= blc(itsLatAxis));
        if (itsSpcAxis >= 0) {
            isGood = isGood && (trc(itsSpcAxis) >= blc(itsSpcAxis));
        }

        if (isGood) {

            itsReadyToExtract = true;

            itsSlicer = casa::Slicer(blc, trc, casa::Slicer::endIsLast);
        }
        else {
            ASKAPLOG_DEBUG_STR(logger, "Extraction box has no size: blc = " << blc << ", trc = " << trc);
            ASKAPLOG_DEBUG_STR(logger, "No extraction done for this source");
        }

        this->initialiseArray();

    }
    else {
        ASKAPLOG_ERROR_STR(logger, "Could not open image");
    }
}

void CubeletExtractor::initialiseArray()
{
    if (this->openInput()) {
        int lngsize = itsSlicer.length()(itsLngAxis);
        int latsize = itsSlicer.length()(itsLatAxis);
        int spcsize = 0;
        if (itsSpcAxis >= 0) {
            spcsize = itsSlicer.length()(itsSpcAxis);
        }
        itsInputShape(itsLngAxis) = lngsize;
        itsInputShape(itsLatAxis) = latsize;
        if (itsSpcAxis >= 0) {
            itsInputShape(itsSpcAxis) = spcsize;
        }
        ASKAPLOG_DEBUG_STR(logger,
                           "Cubelet extraction: Initialising array to zero with shape " <<
                           itsInputShape);
        itsArray = casa::Array<Float>(itsInputShape, 0.0f);
        itsMask = casa::LogicalArray(itsInputShape, true);
    }
    else {
        ASKAPLOG_ERROR_STR(logger, "Could not open image");
    }
}

void CubeletExtractor::extract()
{
    this->defineSlicer();
    if (this->openInput() && itsReadyToExtract) {

        ASKAPLOG_INFO_STR(logger,
                          "Extracting cubelet from " << itsInputCube <<
                          " surrounding source ID " << itsSourceID <<
                          " with slicer " << itsSlicer);

        boost::shared_ptr<accessors::IImageAccess<casacore::Float> > ia = accessors::imageAccessFactory(itsParset);

        itsArray = ia->read(itsInputCube, itsSlicer.start(), itsSlicer.end());
        itsMask  = ia->readMask(itsInputCube, itsSlicer.start(), itsSlicer.end());

    }
    else {
        ASKAPLOG_ERROR_STR(logger, "Could not open image");
    }
}

void CubeletExtractor::writeImage()
{

    itsInputCube = itsInputCubeList[0];
    if (this->openInput() && itsSourceInBounds && itsReadyToExtract) {

        ASKAPLOG_INFO_STR(logger, "Writing cube cutout to " << itsOutputFilename);

        casa::CoordinateSystem newcoo;
        if (itsStkAxis >= 0) {
            newcoo = casa::CoordinateUtil::defaultCoords4D();
        }
        else {
            newcoo = casa::CoordinateUtil::defaultCoords3D();
        }

        int dirCoNum = itsInputCoords.findCoordinate(casa::Coordinate::DIRECTION);
        casa::DirectionCoordinate dircoo(itsInputCoords.directionCoordinate(dirCoNum));
        newcoo.replaceCoordinate(dircoo, newcoo.findCoordinate(casa::Coordinate::DIRECTION));

        if (itsSpcAxis >= 0) {
            int spcCoNum = itsInputCoords.findCoordinate(casa::Coordinate::SPECTRAL);
            casa::SpectralCoordinate spcoo(itsInputCoords.spectralCoordinate(spcCoNum));
            newcoo.replaceCoordinate(spcoo, newcoo.findCoordinate(casa::Coordinate::SPECTRAL));
        }

        casa::Vector<Int> stkvec(itsStokesList.size());
        if (itsStkAxis >= 0) {
            for (size_t i = 0; i < stkvec.size(); i++) {
                stkvec[i] = itsStokesList[i];
            }
            casa::StokesCoordinate stkcoo(stkvec);
            newcoo.replaceCoordinate(stkcoo, newcoo.findCoordinate(casa::Coordinate::STOKES));
        }

        // shift the reference pixel for the spatial coords, so that
        // the RA/DEC (or whatever) are correct. Leave the
        // spectral/stokes axes untouched.
        casa::IPosition outshape(itsSlicer.ndim(), 1);
        int lngAxis = newcoo.directionAxesNumbers()[0];
        int latAxis = newcoo.directionAxesNumbers()[1];
        outshape(lngAxis) = itsSlicer.length()(itsLngAxis);
        outshape(latAxis) = itsSlicer.length()(itsLatAxis);
        if (itsSpcAxis >= 0) {
            int spcAxis = newcoo.spectralAxisNumber();
            outshape(spcAxis) = itsSlicer.length()(itsSpcAxis);
        }
        if (itsStkAxis >= 0) {
            int stkAxis = newcoo.polarizationAxisNumber();
            outshape(stkAxis) = stkvec.size();
        }
        casa::Vector<Float> shift(outshape.size(), 0);
        casa::Vector<Float> incrFac(outshape.size(), 1);
        shift(lngAxis) = itsSource->getXmin() - itsSpatialPad + itsSource->getXOffset();
        shift(latAxis) = itsSource->getYmin() - itsSpatialPad + itsSource->getYOffset();
        if (itsSpcAxis >= 0) {
            int spcAxis = newcoo.spectralAxisNumber();
            shift(spcAxis) = itsSource->getZmin() - itsSpectralPad + itsSource->getZOffset();
        }
        casa::Vector<Int> newshape = outshape.asVector();

        newcoo.subImageInSitu(shift, incrFac, newshape);

        Array<Float> newarray(itsArray.reform(outshape));
        LogicalArray newmask(itsMask.reform(outshape));

        boost::shared_ptr<accessors::IImageAccess<casacore::Float> > ia = accessors::imageAccessFactory(itsParset);
        ia->create(itsOutputFilename, newarray.shape(), newcoo);

        // write the array
        ia->write(itsOutputFilename, newarray);

        // write the flux units
        ia->setUnits(itsOutputFilename, itsInputUnits);

        ia->writeMask(itsOutputFilename, newmask);

        this->writeBeam(itsOutputFilename);
        updateHeaders(itsOutputFilename);

        if (itsInputIsMasked) {
            // copy the image mask to the cubelet, if there is one.
            // casa::LogicalArray
            casa::LogicalArray mask(ia->readMask(itsInputCube)(itsSlicer).reform(outshape));
            ia->makeDefaultMask(itsOutputFilename);
            ia->writeMask(itsOutputFilename, mask, casa::IPosition(outshape.nelements(), 0));
        }

    }
    else {
        ASKAPLOG_ERROR_STR(logger, "Could not open image");
    }
}

}

}
