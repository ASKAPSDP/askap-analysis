/// @file
///
/// Handle the parameterisation of objects that require reading from a file on disk
///
/// @copyright (c) 2014 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Matthew Whiting <Matthew.Whiting@csiro.au>
///
#include <askap/analysis/parallelanalysis/DistributedContinuumParameterisation.h>
#include <askap/analysis/parallelanalysis/DistributedParameteriserBase.h>

#include <askap_analysis.h>

#include <askap/askap/AskapLogging.h>
#include <askap/askap/AskapError.h>
#include <askap/askapparallel/AskapParallel.h>

#include <askap/analysis/catalogues/CasdaIsland.h>
#include <askap/analysis/catalogues/CasdaComponent.h>
#include <askap/analysis/catalogues/CasdaPolarisationEntry.h>
#include <askap/analysis/catalogues/Casda.h>
#include <askap/components/AskapComponentImager.h>
#include <askap/imageaccess/CasaImageAccess.h>
#include <askap/imageaccess/ImageAccessFactory.h>
#include <askap/analysis/extraction/SourceDataExtractor.h>
#include <askap/analysis/extraction/SourceSpectrumExtractor.h>
#include <askap/analysis/extraction/NoiseSpectrumExtractor.h>

#include <casacore/casa/Arrays/Array.h>
#include <Blob/BlobString.h>
#include <Blob/BlobIBufString.h>
#include <Blob/BlobOBufString.h>
#include <Blob/BlobIStream.h>
#include <Blob/BlobOStream.h>
using namespace LOFAR::TYPES;

///@brief Where the log messages go.
ASKAP_LOGGER(logger, ".distribcontparam");

namespace askap {
namespace analysis {

DistributedContinuumParameterisation::DistributedContinuumParameterisation(askap::askapparallel::AskapParallel& comms,
                                                                           const LOFAR::ParameterSet &parset,
                                                                           std::vector<sourcefitting::RadioSource> sourcelist):
    DistributedParameteriserBase(comms, parset, sourcelist),
    itsIslandList(),
    itsComponentList(),
    itsPolComponentList(),
    itsDoRMsynth(parset.getBool("RMSynthesis", false)),
    itsDoExtractSourceSpectra(parset.getBool("Components.extractSpectra",false)),
    itsDoExtractNoiseSpectra(parset.getBool("Components.extractNoiseSpectra",false))

{
    std::string inputImage = parset.getString("image","");
    ASKAPCHECK(inputImage != "", "No image name provided in parset with parameter 'image'");

    itsInputSlicer = analysisutilities::subsectionToSlicer(itsDP->cube().pars().section());
    analysisutilities::fixSlicer(itsInputSlicer, itsDP->cube().header().getWCS());
    
    casa::IPosition arrshape = itsInputSlicer.end();
    arrshape -= itsInputSlicer.start();
    arrshape += 1;
    itsComponentImage = casa::Array<float>(arrshape,0.f);
}

DistributedContinuumParameterisation::~DistributedContinuumParameterisation()
{
}


void DistributedContinuumParameterisation::parameterise()
{

    if (itsComms->isWorker()) {

        // Master does not need to do this, as we get one island per
        // RadioSource object, so comparison can be done with input list.
        std::vector<sourcefitting::RadioSource>::iterator obj;
        for (obj = itsInputList.begin(); obj != itsInputList.end(); obj++) {
            CasdaIsland island(*obj, itsReferenceParset);
            itsIslandList.push_back(island);
            std::vector<casa::Gaussian2D<Double> > gaussians = obj->gaussFitSet(casda::componentFitType);
            ASKAPASSERT(gaussians.size() == obj->numFits());
            for (size_t i = 0; i < obj->numFits(); i++) {
                CasdaComponent component(*obj, itsReferenceParset, i, casda::componentFitType);
                itsComponentList.push_back(component);
                
                addToComponentImage(gaussians[i]);

            }
            
        }

    }

}

void DistributedContinuumParameterisation::addToComponentImage(casa::Gaussian2D<Double> &gauss)
{

    // Calculate region of influence for Gaussian - where is its flux >0?
    //
    float majorSigma = gauss.majorAxis() / (2. * M_SQRT2 * sqrt(M_LN2));
    float zeroPoint = majorSigma * sqrt(-2.*log(1. / (MAXFLOAT * gauss.height())));
    int xmin = std::max(lround(gauss.xCenter() - zeroPoint), 0L);
    int xmax = std::min(lround(gauss.xCenter() + zeroPoint), long(itsComponentImage.shape()[0] - 1));
    int ymin = std::max(lround(gauss.yCenter() - zeroPoint), 0L);
    int ymax = std::min(lround(gauss.yCenter() + zeroPoint), long(itsComponentImage.shape()[1] - 1));

    // casa::Vector<double> pos(2);
    casa::IPosition loc(itsComponentImage.ndim(),0);
    for (int y=ymin; y<=ymax; y++){
        for (int x=xmin; x<=xmax; x++){
            loc[0]=x;
            loc[1]=y;
            // pos(0)=x*1.;
            // pos(1)=y*1.;
            // itsComponentImage(loc) += gauss(pos);
            itsComponentImage(loc) += askap::components::AskapComponentImager::evaluateGaussian(gauss, x, y);
        }
    }
    
}

void DistributedContinuumParameterisation::rmsynthesis()
{
    const bool useFitsTable = itsReferenceParset.getBool("RMSynthesis.usefitstable",false);
    if (itsDoRMsynth) {
        const int master = 0;
        if (itsComms->isWorker()) {
            for(size_t i=0;i<itsComponentList.size();i++){
                CasdaPolarisationEntry pol(&(itsComponentList[i]),itsReferenceParset);
                if (pol.polAvailable()) {
                    itsPolComponentList.push_back(pol);
                }
            }
            if ( useFitsTable ) {
                for (auto& pe : itsPolComponentList) {
                    if ( pe.isAbovePintFitSNR() ) {
                        this->sendFaradayDispersionAndRotationData(pe);
                    }
                }
                // send 0 to tell master that we have finished sending
                std::size_t end = 0;
                itsComms->send(&end,sizeof(std::size_t),master);

            }
        }
        if ( itsComms->isMaster() && useFitsTable ) {
            this->receiveFaradayDispersionAndRotationData();
        }

        if ( useFitsTable ) {
            itsComms->barrier();
            // now do the I, Q, U and V stokes
            if ( itsComms->isWorker() ) {
                std::size_t end = 0;
                // Because of the way the send and receive methods are written, sending and receiving
                // of each stokes have to be in separate for loop. "Should" look at the code again
                // and try to send and receive all the stokes in one for loop.
                for(auto& polCompEntry : itsPolComponentList){
                    if ( polCompEntry.isAbovePintFitSNR() ) {
                        std::shared_ptr<PolarisationData> polData = polCompEntry.getPolData();
                        StokesSpectrum& I = polData->stokesI();
                        this->sendStokes(I);
                    }
                }
                itsComms->send(&end,sizeof(std::size_t),0);
                for(auto& polCompEntry : itsPolComponentList){
                    if ( polCompEntry.isAbovePintFitSNR() ) {
                        std::shared_ptr<PolarisationData> polData = polCompEntry.getPolData();
                        StokesSpectrum& Q = polData->stokesQ();
                        this->sendStokes(Q);
                    }
                }
                // send 0 to signal that we are finished
                itsComms->send(&end,sizeof(std::size_t),0);
                for(auto& polCompEntry : itsPolComponentList){
                    if ( polCompEntry.isAbovePintFitSNR() ) {
                        std::shared_ptr<PolarisationData> polData = polCompEntry.getPolData();
                        StokesSpectrum& U = polData->stokesU();
                        this->sendStokes(U);
                    }
                }
                itsComms->send(&end,sizeof(std::size_t),0);
                for(auto& polCompEntry : itsPolComponentList){
                    if ( polCompEntry.isAbovePintFitSNR() ) {
                        std::shared_ptr<PolarisationData> polData = polCompEntry.getPolData();
                        StokesSpectrum& V = polData->stokesV();
                        this->sendStokes(V);
                    }
                }
                itsComms->send(&end,sizeof(std::size_t),0);
            } else { // master
                this->receiveStokes(); // I
                ASKAPLOG_DEBUG_STR(logger, "Completed extracting I stokes");
                this->receiveStokes(); // Q
                ASKAPLOG_DEBUG_STR(logger, "Completed extracting I stokes");
                this->receiveStokes(); // U
                ASKAPLOG_DEBUG_STR(logger, "Completed extracting Q stokes");
                this->receiveStokes(); // V
                ASKAPLOG_DEBUG_STR(logger, "Completed extracting V stokes");
            }
            ASKAPLOG_DEBUG_STR(logger, "rank: " << itsComms->rank() << " got to barrier.");
            itsComms->barrier();
        }
    }
}

void DistributedContinuumParameterisation::sendStokes(StokesSpectrum& stokes)
{
    if ( itsComms->isWorker() ) {
        boost::shared_ptr<SourceSpectrumExtractor> sse = stokes.specExtractor(); 
        boost::shared_ptr<NoiseSpectrumExtractor> nse = stokes.noiseExtractor();
        std::string cubeName = stokes.cubeName();
        std::string componentID = stokes.componentID();
        //std::string componentID = stokes.componentPart();
        std::string fitsFileName = sse->outputFileBase() + "_" + std::string(stokes.idBase(),0,stokes.idBase().size()-1);
        std::string fitsNoiseFileName = nse->outputFileBase() + "_" + std::string(stokes.idBase(),0,stokes.idBase().size()-1);
        casa::Array<Float> spectrum = sse->array();
        casa::Array<Float> noise = nse->array();

        std::size_t len = 0;
        const int master = 0;
        // send the length of the cube name
        len = cubeName.size();
        itsComms->send(&len,sizeof(std::size_t),master);
        // send the name 
        itsComms->send(cubeName.data(),len,master);
        // send the len of the component id
        len = componentID.size();
        itsComms->send(&len,sizeof(std::size_t),master);
        // send the compenent id
        itsComms->send(componentID.data(),len,master);
        // send the fits file for the spectrum
        len = fitsFileName.size();
        itsComms->send(&len,sizeof(std::size_t),master);
        itsComms->send(fitsFileName.data(),len,master);
        // send the fits file for the noise spectrum
        len = fitsNoiseFileName.size();
        itsComms->send(&len,sizeof(std::size_t),master);
        itsComms->send(fitsNoiseFileName.data(),len,master);
        // send the size of the spectrum
        len = spectrum.nelements();
        itsComms->send(&len,sizeof(std::size_t),master);
        // send the spectrum
        itsComms->send(spectrum.tovector().data(),len*sizeof(casacore::Float),master);
        // send the noise spectrum
        len = noise.nelements();
        itsComms->send(&len,sizeof(std::size_t),master);
        itsComms->send(noise.tovector().data(),len*sizeof(casacore::Float),master);
    }
}

void DistributedContinuumParameterisation::receiveStokes()
{
    if ( itsComms->isMaster() ) {
        const int nProcs = itsComms->nProcs();
        std::size_t len = 0;
        for ( int r = 1; r < nProcs; r++) {
            // receive the length of the cube name
            while ( true ) {
                itsComms->receive(&len,sizeof(std::size_t),r);
                if ( len == 0 ) {
                    break; // while loop
                }

                std::vector<char> temp(len);
                // receive the cube name 
                itsComms->receive(temp.data(),len,r);
                std::string cubeName(temp.data(),len);
                // receive the length of the component id
                itsComms->receive(&len,sizeof(std::size_t),r);
                temp.resize(len);
                // receive component id 
                itsComms->receive(temp.data(),len,r);
                std::string compId(temp.data(),len);
                // receive the fits file name for the spectrum
                itsComms->receive(&len,sizeof(std::size_t),r);
                temp.resize(len);
                itsComms->receive(temp.data(),len,r);
                std::string fitsSpectrumFileName(temp.data(),len);
                // receive the fits file name for the noise spectrum
                itsComms->receive(&len,sizeof(std::size_t),r);
                temp.resize(len);
                itsComms->receive(temp.data(),len,r);
                std::string fitsNoiseSpectrumFileName(temp.data(),len);     
                // receive the size of the spectrum
                itsComms->receive(&len,sizeof(std::size_t),r);
                //std::vector<float> spectrum(len);
                casacore::Vector<float> spectrum(len);
                float* specData = spectrum.data();
                itsComms->receive(specData,sizeof(float)*len,r);
                // receive the size of the noise spectrum
                itsComms->receive(&len,sizeof(std::size_t),r);
                //std::vector<float> noiseSpectrum(len);
                casacore::Vector<float> noiseSpectrum(len);
                float* noise = noiseSpectrum.data();
                itsComms->receive(noise,sizeof(float)*len,r);
                

                boost::shared_ptr<accessors::IImageAccess<casacore::Float> > ia = accessors::imageAccessFactory(itsReferenceParset);
                std::shared_ptr<askap::accessors::FitsAuxImageSpectra> specWriter;
                std::shared_ptr<askap::accessors::FitsAuxImageSpectra> noiseWriter;
                auto iter = itsWriterMap.find(fitsSpectrumFileName);
                if ( iter == itsWriterMap.end() ) {
                    ASKAPLOG_INFO_STR(logger,"creating spectra fits file: " << fitsSpectrumFileName);
                    casacore::CoordinateSystem coord = ia->coordSys(cubeName);
                    specWriter.reset(new askap::accessors::FitsAuxImageSpectra(fitsSpectrumFileName,spectrum.size(),0,coord));
                    itsWriterMap.insert(std::make_pair(fitsSpectrumFileName,specWriter));
                    
                } else {
                    specWriter = iter->second;
                }
                auto noiseIter = itsWriterMap.find(fitsNoiseSpectrumFileName);
                if ( noiseIter == itsWriterMap.end() ) {
                    ASKAPLOG_INFO_STR(logger,"creating noise fits file: " << fitsNoiseSpectrumFileName);
                    casacore::CoordinateSystem coord = ia->coordSys(cubeName);
                    noiseWriter.reset(new askap::accessors::FitsAuxImageSpectra(fitsNoiseSpectrumFileName,spectrum.size(),0,coord));
                    itsWriterMap.insert(std::make_pair(fitsNoiseSpectrumFileName,noiseWriter));
                } else {
                    noiseWriter = noiseIter->second;
                }

                specWriter->add(compId,spectrum);
                noiseWriter->add(compId,noiseSpectrum);
            } // while
        }
    }
}


void DistributedContinuumParameterisation::gatherAndWrite()
{
    // This method should be execute by the master rank.
    if ( itsComms->isMaster() ) {
        LOFAR::ParameterSet extractSubset = itsReferenceParset.makeSubset("Components.extractSpectra.");
        bool useSpectraTable = extractSubset.getBool("usefitstable",false);
        boost::shared_ptr<SpectralBoxExtractor> extractor;
        if (itsDoExtractSourceSpectra && useSpectraTable) {
            extractor = boost::shared_ptr<SpectralBoxExtractor>(new SourceSpectrumExtractor(extractSubset,itsComms)); 
            // gather the spectra from the workers and write to fits binary table
            extractor->gatherSources();
        }
        extractSubset = itsReferenceParset.makeSubset("Components.extractNoiseSpectra.");
        useSpectraTable = extractSubset.getBool("usefitstable",false);
        if (itsDoExtractNoiseSpectra && useSpectraTable) {
            extractor = boost::shared_ptr<SpectralBoxExtractor>( new NoiseSpectrumExtractor(extractSubset,itsComms));    
            // gather the noise spectra from the workers and write to fits binary table
            extractor->gatherSources();
        }
    }
}

void DistributedContinuumParameterisation::extract()
{
    if (itsComms->isWorker()) {
        int master = 0;

        LOFAR::ParameterSet extractSubset = itsReferenceParset.makeSubset("Components.extractSpectra.");
        bool useSpectraTable = extractSubset.getBool("usefitstable",false);
        float fluxLimit = itsReferenceParset.getFloat("Components.extractSpectra.fluxLimit",0.);
        if (itsDoExtractSourceSpectra) {
            for (size_t i=0; i<itsComponentList.size(); i++) {
                if(itsComponentList[i].intFlux() > fluxLimit){
                    boost::shared_ptr<SourceDataExtractor> extractor = boost::shared_ptr<SourceDataExtractor>(
                        new SourceSpectrumExtractor(extractSubset,itsComms));
                    ASKAPLOG_DEBUG_STR(logger, "Extracting spectrum for component " << itsComponentList[i].componentID());
                    extractor->setSource(&itsComponentList[i]);
                    extractor->extract();
                    extractor->writeImage();
                }
            }
            // tell the master the we finish ssending the spectra data even if it has no components 
            // because the master sits and waits for all ranks to finish
            if ( useSpectraTable ) {
                std::size_t len = 0;
                itsComms->send(&len,sizeof(std::size_t),master);
            }
        }

        useSpectraTable = extractSubset.getBool("usefitstable",false);
        extractSubset = itsReferenceParset.makeSubset("Components.extractNoiseSpectra.");
        float noiseFluxLimit = itsReferenceParset.getFloat("Components.extractNoiseSpectra.fluxLimit",0.);
        if (itsDoExtractNoiseSpectra) {
            for (size_t i=0; i<itsComponentList.size(); i++) {
                if(itsComponentList[i].intFlux() > noiseFluxLimit){
                    boost::shared_ptr<SourceDataExtractor> extractor = boost::shared_ptr<SourceDataExtractor>(
                        new NoiseSpectrumExtractor(extractSubset,itsComms));
                    ASKAPLOG_DEBUG_STR(logger, "Extracting noise spectrum for component " << itsComponentList[i].componentID());
                    extractor->setSource(&itsComponentList[i]);
                    extractor->extract();
                    extractor->writeImage();
                }
            }
            // tell the master the we finish ssending the noise spectra data if it has no components
            // because the master sits and waits for all ranks to finish
            if ( useSpectraTable ) {
                std::size_t len = 0;
                itsComms->send(&len,sizeof(std::size_t),master);
            }
        }

    }
}

void DistributedContinuumParameterisation::sendFaradayDispersionAndRotationData(CasdaPolarisationEntry& pe)
{
    const int master = 0;
    std::string inputCube = "";
    double phi = 0.0;
    double deltaPhi = 0.0;
    double phi_rmsf = 0.0;
    std::shared_ptr<RMSynthesis> rmsSynthesis = pe.getRMSythesis();
    std::size_t len = 0;
    // send the component id
    len = pe.id().size();
    // len = pe.componentPart().size();
    itsComms->send(&len,sizeof(std::size_t),master);
    itsComms->send(pe.id().data(),len,master);
    // send the base output file name
    std::shared_ptr<PolarisationData> polData = pe.getPolData();
    std::string baseOutputFile = polData->stokesI().idBase();
    len = baseOutputFile.size();
    itsComms->send(&len,sizeof(std::size_t),master);
    itsComms->send(baseOutputFile.data(),len,master);

    // send the length of the Faraday Dispersion Function vector
    //const std::vector<casa::Complex>& fdf = rmsSynthesis->fdf().tovector();
    const casacore::Vector<casa::Complex>& fdf = rmsSynthesis->fdf();
    len = fdf.size();
    itsComms->send(&len,sizeof(std::size_t),master);
    // send the content of Faraday Dispersion Function vector
    itsComms->send(fdf.data(),len*sizeof(casa::Complex),master);

    // send the length of the Rotation Measure Spread function array
    //const std::vector<casa::Complex>& rmsf = rmsSynthesis->rmsf().tovector();
    const casacore::Vector<casa::Complex>& rmsf = rmsSynthesis->rmsf();
    len = rmsf.size();
    itsComms->send(&len,sizeof(std::size_t),master);
    // now send its content
    itsComms->send(rmsf.data(),len*sizeof(casa::Complex),master);    
    if ( itsComms->rank() == 1 ) {
        inputCube = rmsSynthesis->inputCube();
        ASKAPCHECK(inputCube != "","input cube cant be empty");
        // send the length of the input cube name
        len = inputCube.size();
        itsComms->send(&len,sizeof(std::size_t),master);
        // send the name of the input cube
        itsComms->send(inputCube.data(),len,master);
        // send the phi
        phi = static_cast<double>(rmsSynthesis->phi()[0]);
        itsComms->send(&phi,sizeof(double),master);
        // send deltaphi
        deltaPhi = static_cast<double>(rmsSynthesis->deltaPhi());
        itsComms->send(&deltaPhi,sizeof(double),master);
        // send phi_rmsf
        phi_rmsf = static_cast<double>(rmsSynthesis->phi_rmsf()[0]);
        itsComms->send(&phi_rmsf,sizeof(double),master);
    }
}

void DistributedContinuumParameterisation::receiveFaradayDispersionAndRotationData()
{
    std::string inputCube = "";
    double phi = 0.0;
    double deltaPhi = 0.0;
    double phi_rmsf = 0.0;

    const int nProcs = itsComms->nProcs();
    std::shared_ptr<askap::accessors::FitsAuxImageSpectra> fdfAmpWriter;
    std::shared_ptr<askap::accessors::FitsAuxImageSpectra> fdfPhaseWriter;
    std::shared_ptr<askap::accessors::FitsAuxImageSpectra> rmsfAmpWriter;
    std::shared_ptr<askap::accessors::FitsAuxImageSpectra> rmsfPhaseWriter;

    for ( int r = 1; r < nProcs; r++) {
        while ( true ) {
            // receive the length of the stoke spectrum
            std::size_t len = 0;
            // receive component id len
            itsComms->receive(&len,sizeof(std::size_t),r);
            if ( len != 0 ) {
                // receive the name of the component part of the component id
                std::vector<char> tmp(len);
                itsComms->receive(tmp.data(),len,r);
                std::string componentID(tmp.data(),len);

                // receive the base output file name
                itsComms->receive(&len,sizeof(std::size_t),r);
                tmp.resize(len);
                itsComms->receive(tmp.data(),len,r);
                // minus 1 to remove the '_' at the end
                std::string baseName(tmp.data(),len-1);
                // receive the length of Faraday Dispersion Function vector
                itsComms->receive(&len,sizeof(std::size_t),r);
                // receive the content of the Faraday Dispersion Function vector
                //std::vector<casa::Complex> fdf(len);
                casacore::Vector<casa::Complex> fdf(len);
                casacore::Complex* fdfData = fdf.data();
                itsComms->receive(fdfData,len*sizeof(casacore::Complex),r);

                // receive the length of the Rotation Measure Spread function array
                itsComms->receive(&len,sizeof(std::size_t),r);
                // now receive Rotation Measure Spread function array data
                //std::vector<casa::Complex> rmsf(len);
                casacore::Vector<casa::Complex> rmsf(len);
                casacore::Complex* rmsfData = rmsf.data();
                itsComms->receive(rmsfData,len*sizeof(casacore::Complex),r);

                if ( r == 1 ) {
                    // receive the input cube len
                    itsComms->receive(&len,sizeof(std::size_t),r);
                    std::vector<char> tmp(len);
                    // receive the name of the input cube. @todo - not efficient
                    itsComms->receive(tmp.data(),len,r);
                    inputCube = std::string(tmp.data(),len);
                    // receive phi
                    itsComms->receive(&phi,sizeof(double),r);
                    // receive delta phi
                    itsComms->receive(&deltaPhi,sizeof(double),r);
                    // receive phi_rmsf
                    itsComms->receive(&phi_rmsf,sizeof(double),r);
                }
                // dont know if this is efficient - doubt it ?
                ASKAPLOG_DEBUG_STR(logger,"component id: " << componentID);
                casacore::Vector<float> ampArrFdf = casacore::amplitude(fdf);
                casacore::Vector<float> phaseArrFdf = casacore::phase(fdf);
                casacore::Vector<casacore::Complex> casaVectRmsf(rmsf);
                casacore::Vector<float> ampArrRmsf = casacore::amplitude(rmsf);
                casacore::Vector<float> phaseArrRmsf = casacore::phase(rmsf);

                casacore::CoordinateSystem coordSysForFDF;
                casacore::CoordinateSystem coordSysForRMSF;

                std::string fileNameFdfAmp = itsReferenceParset.getString("RMSynthesis.outputBase", "") + "_FDF_amp_" + baseName;
                std::string fileNameFdfPhase = itsReferenceParset.getString("RMSynthesis.outputBase", "") + "_FDF_phase_" + baseName;
                auto iter = itsWriterMap.find(fileNameFdfAmp);
                auto iter2 = itsWriterMap.find(fileNameFdfPhase);
                if ( iter == itsWriterMap.end() ) {
                    casacore::CoordinateSystem coordSysForFDF;
                    boost::shared_ptr<accessors::IImageAccess<casacore::Float> > ia = accessors::imageAccessFactory(itsReferenceParset);
                    const casa::CoordinateSystem inputcoords = ia->coordSys(inputCube);
                    const int dirCoNum = inputcoords.findCoordinate(casa::Coordinate::DIRECTION);
                    casa::DirectionCoordinate dircoo(inputcoords.directionCoordinate(dirCoNum));
                    coordSysForFDF.addCoordinate(dircoo);
                    coordSysForRMSF.addCoordinate(dircoo);
                    casa::Vector<Double> crpix(1); crpix = 0.0;
                    casa::Vector<Double> crval(1); crval = phi;
                    casa::Vector<Double> cdelt(1); cdelt = deltaPhi;
                    casa::Matrix<Double> pc(1, 1); pc = 0; pc.diagonal() = 1.0;
                    casa::Vector<String> name(1);  name = "Faraday depth";
                    casa::Vector<String> units(1); units = "rad/m2";
                    casacore::LinearCoordinate FDcoordFDF(name, units, crval, cdelt, pc, crpix);
                    coordSysForFDF.addCoordinate(FDcoordFDF);

                    // Then for the RMSF - this should be twice the length, so only the crval changes
                    crval = phi_rmsf;
                    casacore::LinearCoordinate FDcoordRMSF(name, units, crval, cdelt, pc, crpix);
                    coordSysForRMSF.addCoordinate(FDcoordRMSF);

                    fdfAmpWriter.reset(new askap::accessors::FitsAuxImageSpectra(fileNameFdfAmp,ampArrFdf.nelements(),0,coordSysForFDF));
                    fdfPhaseWriter.reset(new askap::accessors::FitsAuxImageSpectra(fileNameFdfPhase,phaseArrFdf.nelements(),0,coordSysForFDF));
                    itsWriterMap.insert(std::make_pair(fileNameFdfAmp,fdfAmpWriter));
                    itsWriterMap.insert(std::make_pair(fileNameFdfPhase,fdfPhaseWriter));
                } else {
                    fdfAmpWriter = iter->second;
                    fdfPhaseWriter = iter2->second;
                }
                fdfAmpWriter->add(componentID,ampArrFdf);
                fdfPhaseWriter->add(componentID,phaseArrFdf);

                std::string fileNameRmsfAmp = itsReferenceParset.getString("RMSynthesis.outputBase", "") + "_RMSF_amp_" + baseName;
                std::string fileNameRmsfPhase = itsReferenceParset.getString("RMSynthesis.outputBase", "") + "_RMSF_phase_" + baseName;
                auto iter3 = itsWriterMap.find(fileNameRmsfAmp);
                auto iter4 = itsWriterMap.find(fileNameRmsfPhase);

                if ( iter == itsWriterMap.end() ) {
                    rmsfAmpWriter.reset(new askap::accessors::FitsAuxImageSpectra(fileNameRmsfAmp,ampArrRmsf.nelements(),0,coordSysForRMSF));
                    rmsfPhaseWriter.reset(new askap::accessors::FitsAuxImageSpectra(fileNameRmsfPhase,phaseArrRmsf.nelements(),0,coordSysForRMSF));
                    itsWriterMap.insert(std::make_pair(fileNameRmsfAmp,rmsfAmpWriter));
                    itsWriterMap.insert(std::make_pair(fileNameRmsfPhase,rmsfPhaseWriter));

                } else {
                    rmsfAmpWriter = iter3->second;
                    rmsfPhaseWriter = iter4->second;
                }
                rmsfAmpWriter->add(componentID,ampArrRmsf);
                rmsfPhaseWriter->add(componentID,phaseArrRmsf);
            } else {
                break; // while loop
            }
        }
    }
}

void DistributedContinuumParameterisation::gather()
{
    if (itsComms->isParallel()) {

        if (itsTotalListSize > 0) {

            if (itsComms->isMaster()) {
                // for each worker, read completed objects until we get a 'finished' signal

                // now read back the sources from the workers
                LOFAR::BlobString bs;
                for (int n = 0; n < itsComms->nProcs() - 1; n++) {
                    int numIslands,numComponents,numPolComponents;
                    itsComms->receiveBlob(bs, n + 1);
                    LOFAR::BlobIBufString bib(bs);
                    LOFAR::BlobIStream in(bib);
                    int version = in.getStart("Contfinal");
                    ASKAPASSERT(version == 1);
                    in >> numIslands;
                    for (int i = 0; i < numIslands; i++) {
                        CasdaIsland isle;
                        in >> isle;
                        itsIslandList.push_back(isle);
                    }
                    in >> numComponents;
                    for (int i = 0; i < numComponents; i++) {
                        CasdaComponent comp;
                        in >> comp;
                        itsComponentList.push_back(comp);
                    }
                    in >> numPolComponents;
                    for (int i = 0; i < numPolComponents; i++) {
                        CasdaPolarisationEntry pol;
                        in >> pol;
                        itsPolComponentList.push_back(pol);
                    }

                    // read image
                    int nelements;
                    int val;
                    in >> nelements;
                    casa::IPosition shape(nelements);
                    for(int i=0;i<nelements;i++){
                        in >> val;
                        shape[i] = val;
                    }
                    std::vector<float> imageAsVector(shape.product(),0.);
                    for(int i=0;i<shape.product();i++){
                        in >> imageAsVector[i];
                    }
                    itsComponentImage += casa::Array<float>(shape,imageAsVector.data());
                        
                    in.getEnd();
                }

                // Make sure we have the correct amount of sources
                ASKAPASSERT(itsInputList.size() == itsIslandList.size());

                // sort by id:
                std::sort(itsIslandList.begin(), itsIslandList.end());
                std::sort(itsComponentList.begin(), itsComponentList.end());
                std::sort(itsPolComponentList.begin(), itsPolComponentList.end());


            } else { // WORKER
                // for each object in itsOutputList, send to master
                LOFAR::BlobString bs;
                bs.resize(0);
                LOFAR::BlobOBufString bob(bs);
                LOFAR::BlobOStream out(bob);
                out.putStart("Contfinal", 1);
                out << int(itsIslandList.size());
                for (size_t i = 0; i < itsIslandList.size(); i++) {
                    out << itsIslandList[i];
                }
                out << int(itsComponentList.size());
                for (size_t i = 0; i < itsComponentList.size(); i++) {
                    out << itsComponentList[i];
                }
                out << int(itsPolComponentList.size());
                for (size_t i = 0; i < itsPolComponentList.size(); i++) {
                    out << itsPolComponentList[i];
                }

                // send image
                casa::IPosition shape = itsComponentImage.shape();
                out << static_cast<casacore::uInt>(shape.nelements());
                for(size_t i=0;i<shape.nelements();i++){
                    int val = shape[i];
                    out << val;
                }
                std::vector<float> imageAsVector=itsComponentImage.tovector();
                for(size_t i=0;i<imageAsVector.size();i++){
                    out << imageAsVector[i];
                }
                out.putEnd();
                itsComms->sendBlob(bs, 0);
            }

        }

    }

}

}


}
