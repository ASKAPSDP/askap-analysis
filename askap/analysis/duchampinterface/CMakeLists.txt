#
# askap/analysis/duchampinterface
#
add_sources_to_analysis(
DuchampInterface.cc
)

install (FILES
DuchampInterface.h
DESTINATION include/askap/analysis/duchampinterface
)

