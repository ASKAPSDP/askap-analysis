add_executable(testHIprofile  testHIprofile.cc)
target_link_libraries(testHIprofile
	askap::analysis
	${CPPUNIT_LIBRARY}
)

install (
    TARGETS testHIprofile 
    EXPORT askap-analysis-targets
    DESTINATION bin
    )


