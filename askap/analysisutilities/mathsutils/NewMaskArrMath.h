/// @file VariableThresholdingHelpers.h
///
/// @copyright (c) 2020 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Matthew Whiting <Matthew.Whiting@csiro.au>
///

#ifndef ASKAP_ANALYSIS_NEW_MASK_ARRAY_MATH_H_
#define ASKAP_ANALYSIS_NEW_MASK_ARRAY_MATH_H_

#include <casacore/casa/aipstype.h>
#include <casacore/casa/Arrays/MaskedArray.h>
#include <casacore/casa/Arrays/MaskArrMath.h>
#include <casacore/casa/namespace.h>

namespace askap {

namespace analysis {

/// @brief Improved version of casacore::MaskedMeanFunc
/// @details This provides a check for the number of valid elements of
/// the MaskedArray in question. If that is zero, the function returns
/// the appropriately templated zero value.
template<typename T> class MaskedMeanFunc {
public:
    T operator() (const MaskedArray<T>& arr) const { if (arr.nelementsValid()>0) return mean(arr); else return T(0); }
};

/// @brief Improved version of casacore::MaskedStddevFunc
/// @details This provides a check for the number of valid elements of
/// the MaskedArray in question. If that is zero, the function returns
/// the appropriately templated zero value.
template<typename T> class MaskedStddevFunc {
public:
    T operator() (const MaskedArray<T>& arr) const { if (arr.nelementsValid()>0) return stddev(arr); else return T(0); }
};

/// @brief Improved version of casacore::MaskedMedianFunc
/// @details This provides a check for the number of valid elements of
/// the MaskedArray in question. If that is zero, the function returns
/// the appropriately templated zero value.
template<typename T> class MaskedMedianFunc {
public:
  explicit MaskedMedianFunc (Bool sorted=False, Bool takeEvenMean=True)
    : itsSorted(sorted), itsTakeEvenMean(takeEvenMean) {}
  T operator() (const MaskedArray<T>& arr) const
        { if (arr.nelementsValid()>0) return median(arr, itsSorted, itsTakeEvenMean); else return T(0); }
private:
  Bool     itsSorted;
  Bool     itsTakeEvenMean;
  Bool     itsInPlace;
};

/// @brief Improved version of casacore::MaskedMadfmFunc
/// @details This provides a check for the number of valid elements of
/// the MaskedArray in question. If that is zero, the function returns
/// the appropriately templated zero value.
template<typename T> class MaskedMadfmFunc {
public:
  explicit MaskedMadfmFunc(Bool sorted=False, Bool takeEvenMean=True)
    : itsSorted(sorted), itsTakeEvenMean(takeEvenMean) {}
  Float operator()(const MaskedArray<Float>& arr) const
        { if (arr.nelementsValid()>0) return madfm(arr, itsSorted, itsTakeEvenMean); else return T(0); }
private:
  Bool     itsSorted;
  Bool     itsTakeEvenMean;
  Bool     itsInPlace;
};





}

}

#endif
